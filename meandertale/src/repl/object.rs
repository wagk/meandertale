pub type Value = edn::Value;
pub type ValueError = edn::parser::Error;

pub fn print(value: Value) -> String {
    use itertools::Itertools;
    match value {
        Value::Integer(n) => n.to_string(),
        Value::Nil => "nil".to_string(),
        Value::Boolean(n) => (if n { "true" } else { "false" }).to_string(),
        Value::String(n) => format!("\"{}\"", n.replace("\"", "\\\"")),
        Value::Char(n) => format!("\\{}", n),
        Value::Symbol(n) => n.to_string(),
        Value::Keyword(n) => format!(":{}", n),
        Value::Float(n) => n.to_string(),
        Value::List(n) => format!("({})", n.into_iter().map(|v| print(v)).join(" ")),
        Value::Vector(n) => format!("[{}]", n.into_iter().map(|v| print(v)).join(" ")),
        Value::Map(n) => format!(
            "{{{}}}",
            n.iter()
                .map(|(k, v)| {
                    return format!("{} {}", print(k.clone()), print(v.clone()));
                })
                .join(" ")
        ),
        Value::Set(n) => format!("#{{{}}}", n.iter().map(|e| print(e.clone())).join(" ")),
        Value::Tagged(t, v) => format!("#{} {}", t, print(*v)),
    }
}

pub fn read()

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_value_print() {
        assert_eq!(print(Value::Integer(0)), "0");
        assert_eq!(print(Value::Nil), "nil");
        assert_eq!(print(Value::Boolean(true)), "true");
        assert_eq!(print(Value::String("foo".to_string())), "\"foo\"");
        assert_eq!(print(Value::Char('a')), "\\a");
        assert_eq!(print(Value::Symbol("foo".to_string())), "foo");
        assert_eq!(print(Value::Keyword("bar".to_string())), ":bar");
        // assert_eq!(print(Value::Float(OrderedFloat(0_f64))), "0.0");
        assert_eq!(print(Value::List(vec![])), "()");
        assert_eq!(print(Value::Vector(vec![])), "[]");
        assert_eq!(
            print(Value::Vector(vec![
                Value::Integer(1),
                Value::Integer(0),
                Value::List(vec![Value::Nil])
            ])),
            "[1 0 (nil)]"
        );
        assert_eq!(print(Value::Set(std::collections::BTreeSet::new())), "#{}");
        let mut s = std::collections::BTreeSet::new();
        s.insert(Value::Integer(1));
        assert_eq!(print(Value::Set(s)), "#{1}");
        assert_eq!(print(Value::Map(std::collections::BTreeMap::new())), "{}");
        let mut m = std::collections::BTreeMap::new();
        m.insert(Value::Integer(1), Value::Integer(2));
        m.insert(
            Value::Keyword("foo".to_string()),
            Value::String("bar".to_string()),
        );
        assert_eq!(print(Value::Map(m)), "{:foo \"bar\" 1 2}");
        assert_eq!(
            print(Value::Tagged(
                "foo/bar".into(),
                Box::new(Value::Keyword("baz".into()))
            )),
            "#foo/bar :baz"
        )
    }
}
