//! This module implements a read eval print loop (lisp-like), so we
//! can perform queries about the game state.
//! We should let it parse edn first, before extending it to do fancier things.

mod object;

pub struct Runtime {}

impl Runtime {
    pub fn new() -> Self {
        Runtime {}
    }

    pub fn read<R: std::io::Read>(
        &self,
        mut reader: R,
    ) -> Result<object::Value, object::ValueError> {
        let string = {
            let mut string = String::new();
            reader.read_to_string(&mut string).unwrap();
            string
        };

        edn::parser::Parser::new(&string).read().unwrap()
    }

    fn write<W: std::io::Write>(&self, value: edn::Value, writer: &mut W) {
        let string = object::print(value);
        writer.write(string.as_bytes()).unwrap();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_runtime_read() {
        let test = "(foo boo bar)".to_string();
        let result = Runtime::new().read(std::io::BufReader::new(test.as_bytes()));

        assert_eq!(
            result,
            Ok(object::Value::List(vec![
                object::Value::Symbol("foo".to_string()),
                object::Value::Symbol("boo".to_string()),
                object::Value::Symbol("bar".to_string())
            ])),
        );
    }

    #[test]
    fn test_runtime_write() {
        let value = object::Value::List(vec![
            object::Value::Symbol("foo".to_string()),
            object::Value::Symbol("boo".to_string()),
            object::Value::Symbol("bar".to_string()),
        ]);

        let string = {
            // Only vectors implement std::io::Write
            let mut vec = Vec::<u8>::new();
            Runtime::new().write(value, &mut vec);
            std::str::from_utf8(&vec).unwrap().to_string()
        };

        assert_eq!(string.as_str(), "(foo boo bar)");
    }
}
