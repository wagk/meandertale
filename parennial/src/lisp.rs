mod form;
mod parser;

pub struct Runtime {}

// TODO: Dummy value that will eventually resolve to whatever type we
// would want the greater runtime to consume
type Value = String;
// TODO: Dummy value that will eventually resolve to whatever type we
// would want to return as an error value
type RuntimeError = String;
// TODO: Dummy value representing the error of a type conversion.
type ValueError = String;

/// This is the main runtime of the lisp interpreter; Its job will
/// include the reading, evaluating, printing, and registration of
/// interoperation we want
impl Runtime {
    pub fn new() -> Self {
        Runtime {}
    }

    /// Takes an expression, and evaluates it (with possible side
    /// effects) in the runtime
    pub fn eval(&mut self, expr: &str) -> Result<Value, RuntimeError> {
        let form = parser::read(expr);
        unimplemented!();
    }

    /// Register foreign functions (if what we're doing is really some
    /// form of FFI integration)
    pub fn register<T>(
        &mut self,
        // Lambda function and name to register
    ) -> bool
    where
        T: Fn(Value) -> Result<Value, ValueError>,
    {
        unimplemented!();
    }
}
