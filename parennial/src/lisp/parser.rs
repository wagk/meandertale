use super::form::Form;
use error::ParseError;
use std::collections::VecDeque;

pub fn read(expr: &str) -> Result<Value, ParseError> {
    unimplemented!();
}

mod error {
    #[derive(Debug)]
    pub struct ParseError {
        what: String,
        error: ParseErrorKind,
    }

    #[derive(Debug)]
    enum ParseErrorKind {
        Regex(regex::Error),
        Read(std::io::Error),
        Syntax,
        Undefined,
    }

    impl ParseError {
        pub fn new() -> Self {
            Self {
                what: String::from("Something's wrong but nobody knows what"),
                error: ParseErrorKind::Undefined,
            }
        }

        pub fn syntax(what: &str) -> Self {
            Self {
                what: String::from(what),
                error: ParseErrorKind::Syntax,
            }
        }
    }

    impl std::convert::From<regex::Error> for ParseError {
        fn from(err: regex::Error) -> Self {
            ParseError {
                what: String::from("Regex error"),
                error: ParseErrorKind::Regex(err),
            }
        }
    }

    impl std::convert::From<std::io::Error> for ParseError {
        fn from(err: std::io::Error) -> Self {
            ParseError {
                what: String::from("Read error"),
                error: ParseErrorKind::Read(err),
            }
        }
    }

    impl std::convert::From<&str> for ParseError {
        fn from(err: &str) -> Self {
            ParseError {
                what: err.to_string(),
                error: ParseErrorKind::Undefined,
            }
        }
    }

    impl std::fmt::Display for ParseError {
        fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
            write!(
                f,
                "ParseError: {}\nParseErrorKind: {:?}",
                self.what, self.error
            )
        }
    }

    impl std::error::Error for ParseError {
        fn description(&self) -> &str {
            self.what.as_str()
        }

        fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
            match &self.error {
                ParseErrorKind::Regex(e) => Some(e),
                ParseErrorKind::Read(e) => Some(e),
                ParseErrorKind::Undefined | ParseErrorKind::Syntax => None,
            }
        }
    }
}

fn tokenize(input: &str) -> Result<Vec<String>, ParseError> {
    let matcher = regex::Regex::new(
        r#"(?x)
                (
                    ~@                 | # capture special dual character characters
                    [()\[\]{}<>'`~^@]  | # capture special single characters
                    "(?:\\.|[^\\"])*"  | # capture strings (including escaped string characters)
                    ;.*                | # capture comments
                    [^\s'",`()\[\]{}]+   # capture symbols and non-special character strings
                )
            "#,
    )?;

    Ok(matcher
        .find_iter(input)
        .map(|range| range.as_str().to_owned())
        .collect::<Vec<String>>())
}

fn is_opening_brace(string: &str) -> bool {
    match string {
        "(" | "{" | "[" => true,
        _ => false,
    }
}

fn is_closing_brace(string: &str) -> bool {
    match string {
        ")" | "}" | "]" => true,
        _ => false,
    }
}

fn read_atom(val: &str) -> Result<Form, ParseError> {
    log::debug!("[read_atom] Entering read_atom");
    if val.is_empty() {
        return Err(error::ParseError::from("Empty token to parse!"));
    }

    assert!(
        !is_closing_brace(val) && !is_opening_brace(val),
        "[read_atom] {:?} an opening, or closing brace",
        val
    );

    log::debug!("[read_atom] reading {:?}", val);

    // Check for keywords
    if let Some(':') = val.chars().next() {
        // TODO: This is really a syntax error, should ParseError handle this?
        // We can just opt to make `:` a valid symbol here, but it
        // sounds pretty pathological to be honest
        return if let None = val.chars().next() {
            Err(ParseError::syntax("Symbols cannot be a single colon (:)"))
        } else {
            // also weird behavior because of unicode
            // https://doc.rust-lang.org/std/string/struct.String.html#method.chars
            Ok(Form::Keyword(String::from(val)))
        };
    }

    // Check for integral values
    if let Ok(integer) = val.to_string().parse::<i64>() {
        return Ok(Form::Integer(integer));
    }

    Ok(Form::Symbol(String::from(val)))
}

fn read_list<T>(iter: &mut std::iter::Peekable<T>) -> Result<Form, error::ParseError>
where
    T: std::iter::Iterator<Item = String>,
{
    log::debug!("[read_list] Entering read_list");
    let mut vec = VecDeque::<Form>::new();
    loop {
        match iter.peek() {
            Some(ref item) if is_closing_brace(&item) => {
                log::debug!("[read_list] Stopping list parsing; found {:?}", item);
                iter.next();
                break;
            }
            Some(item) => {
                log::debug!("[read_list] iter.next() found {:?}, reading form", item);
                vec.push_back(read_form(iter)?);
            }
            None => {
                log::error!("[read_list] found unbalanced parentheses!");
                return Err(ParseError::from("Unbalanced parentheses"));
            }
        }
    }
    Ok(Form::List(vec))
}

/// Reads a form. A form can consist of a list, or an atom.
fn read_form<T>(iter: &mut std::iter::Peekable<T>) -> Result<Form, ParseError>
where
    T: std::iter::Iterator<Item = String>,
{
    log::info!("[read_form] Entering read_form");
    match iter.next() {
        Some(ref item) if is_opening_brace(&item) => {
            log::debug!("[read_form] Starting list parse; found {}", item);
            read_list(iter)
        }
        Some(item) => read_atom(&item),
        None => Err(ParseError::new()),
    }
}

/// Lexing is tokenising a stream of text. Parsing is checking the
/// context in which each token appears.
fn lex(stream: &Vec<String>) -> Result<Form, ParseError> {
    let mut iter = stream.iter().cloned().peekable();
    read_form(&mut iter)
}

#[allow(dead_code)]
pub fn read<R>(reader: &mut R) -> Result<Form, ParseError>
where
    R: std::io::Read,
{
    let input = {
        let mut input = String::new();
        reader.read_to_string(&mut input)?;
        input
    };

    let tokens = tokenize(input.as_str())?;
    Ok(lex(&tokens)?)
}

#[cfg(test)]
mod tests {
    use super::Form::*;
    use super::*;

    fn init_logger() {
        let _ = env_logger::builder()
            .is_test(true)
            .filter(None, log::LevelFilter::Trace)
            .try_init();
    }

    // TODO: Without const generics, we have to make a macro out of
    // this or something
    fn list(list: &[Form]) -> Form {
        List(list.iter().cloned().collect())
    }

    fn symbol(s: &str) -> Form {
        Symbol(s.to_string())
    }

    #[test]
    fn test_read_atom_symbol() {
        let sym = read_atom("symbol").unwrap();
        match sym {
            Symbol(s) => assert_eq!(s, "symbol"),
            _ => panic!("Uncaught symbol case!"),
        }
    }

    #[test]
    #[should_panic]
    fn test_read_atom_empty() {
        read_atom("").unwrap();
    }

    #[test]
    fn test_read_form() {
        init_logger();
        log::debug!("Tokenizing");
        let mut tokens = tokenize("(foo boo bar)").unwrap().into_iter().peekable();
        log::debug!("Tokenized {:?}", tokens);
        if let List(list) = read_form(&mut tokens).unwrap() {
            let correct_parse = vec![symbol("foo"), symbol("boo"), symbol("bar")];
            for (x, correct) in list.iter().zip(correct_parse.iter()) {
                assert_eq!(x, correct);
            }
        } else {
            panic!("read_form did not return a list!")
        }
    }

    #[test]
    fn test_read_nested_form() {
        init_logger();
        let input = "(foo (bar) boo (bar (baz)))";
        let mut tokens = tokenize(input).unwrap().into_iter().peekable();
        let parsed = read_form(&mut tokens).unwrap();

        assert_eq!(
            parsed,
            list(&[
                symbol("foo"),
                list(&[symbol("bar")]),
                symbol("boo"),
                list(&[symbol("bar"), list(&[symbol("baz")])]),
            ])
        );
    }

    #[test]
    fn test_generate_ast() {
        let tokens = tokenize("(foo boo bar)");
        let result = lex(&tokens.unwrap());

        println!("{:?}", result);
    }
}
