// NOTE: HashMap does not implement Hash, for some reason
use std::collections::{BTreeMap, VecDeque};

mod iterator;
use iterator::{IntoIter, Iter};

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
pub enum Form {
    // symbol
    Symbol(String),
    // true, false
    Boolean(bool),
    // nil
    Null,
    // 1
    Integer(i64),
    // :keywords
    Keyword(String),
    // ()
    List(VecDeque<Form>),
    // []
    Vec(Vec<Form>),
    // {}
    Map(BTreeMap<Form, Form>),
}

impl Form {
    /// If the string is empty, panic (since that's illegal)
    /// If the string starts with ":", or is only ":", panic (since
    /// that's keyword stuff)
    pub fn sym(s: &str) -> Option<Self> {
        if s.is_empty() {
            None
        } else {
            Some(Self::Symbol(s.to_string()))
        }
    }

    // /// If the string starts with ':', strip it and create the keyword.
    // /// If the string is empty, panic (since that's illegal)
    // /// If the string is only ":", panic (since that's illegal)
    // pub fn keyword(s: &str) -> Self {
    //     assert!(s.len() > 1);
    //     match s.chars().next() {}
    // }
}

// TODO: Have this take in a proper lisp syntax instead of just basic
// tokens
impl std::convert::From<&str> for Form {
    fn from(s: &str) -> Self {
        match s {
            n if n == "nil" => Self::Null,
            t if t == "true" => Self::Boolean(true),
            f if f == "false" => Self::Boolean(false),
            k if k.starts_with(":") => Self::Keyword(String::from(&k[1..])),
            r if r.starts_with("\"") && r.ends_with("\"") => {
                let _string = String::from(&r[1..(r.len() - 1)]);
                // TODO: escape the thing. Remove one level of escape slashes
                // mal recommends to have a print_readably function
                todo!();
            }
            // TODO: Cases where there's an empty delimiter
            // We should reject:
            // - only a starting/ending string delimiter
            // - standalone string delimiters.
            _ => Self::Symbol(String::from(s)),
        }
    }
}

impl std::convert::From<i64> for Form {
    fn from(i: i64) -> Self {
        Self::Integer(i)
    }
}

// NOTE: We probably can't implement it as a generic impl yet, since
// rust has no variadic generics (in addition to not having const
// generics).

impl std::fmt::Display for Form {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        use itertools::Itertools;
        match self {
            Self::Boolean(b) => {
                if *b {
                    write!(f, "true")
                } else {
                    write!(f, "false")
                }
            }
            Self::Null => write!(f, "nil"),
            Self::Symbol(s) => write!(f, "{}", s),
            Self::Integer(i) => write!(f, "{}", i),
            Self::Keyword(k) => write!(f, ":{}", k),
            Self::Vec(v) => write!(f, "[{}]", v.iter().map(|v| v.to_string()).join(" ")),
            Self::List(vs) => write!(f, "({})", vs.iter().map(|v| v.to_string()).join(" ")),
            Self::Map(m) => write!(
                f,
                "{{{}}}",
                m.iter().map(|(k, v)| format!("{} {}", k, v)).join(" ")
            ),
        }
    }
}

impl std::iter::IntoIterator for Form {
    type Item = Form;
    type IntoIter = IntoIter;
    fn into_iter(self) -> Self::IntoIter {
        Self::IntoIter::new(self)
    }
}

#[cfg(test)]
mod tests {
    use super::Form::*;
    use super::*;
    use maplit::btreemap;

    fn form_eq(ast: Form, s: &str) {
        assert_eq!(format!("{}", ast).as_str(), s);
    }

    fn list(list: &[Form]) -> Form {
        List(list.iter().cloned().collect())
    }

    fn keyword(key: &str) -> Form {
        assert!(
            key.starts_with(":") == false,
            "Internal keyword representation does not require : "
        );
        Form::Keyword(format!("{}", key))
    }

    fn symbol(s: &str) -> Form {
        Form::sym(s).unwrap()
    }

    #[test]
    fn list_to_string() {
        form_eq(list(&[]), "()");
        form_eq(list(&[keyword("foo"), symbol("bar")]), "(:foo bar)")
    }

    #[test]
    fn vec_to_string() {
        form_eq(Vec(vec![]), "[]");
        form_eq(Vec(vec![keyword("foo"), symbol("bar")]), "[:foo bar]")
    }

    #[test]
    fn map_to_string() {
        form_eq(Map(BTreeMap::new()), "{}");
        form_eq(
            Map(btreemap! {
                keyword("foo") => symbol("bar"),
            }),
            "{:foo bar}",
        );
    }

    #[test]
    fn construct_symbol() {
        assert!(matches!(Form::sym("foo"), Some(Form::Symbol(_))));
        assert!(matches!(Form::sym(""), None));
    }

    #[test]
    fn value_display() {
        // Anything that implements Display also implements ToString
        form_eq(symbol("foo"), "foo");
        form_eq(list(&[symbol("foo"), symbol("bar")]), "(foo bar)");
        form_eq(
            list(&[symbol("foo"), list(&[symbol("bar"), symbol("baz")])]),
            "(foo (bar baz))",
        );
    }
}
