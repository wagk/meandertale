use super::Form;
use std::{boxed::Box, iter::once};

pub struct Iter<'a> {
    context: Box<dyn std::iter::Iterator<Item = &'a Form> + 'a>,
}

impl<'a> Iter<'a> {
    pub fn new(form: &'a Form) -> Self {
        Self {
            context: match form {
                Form::Map(m) => Box::new(m.iter().flat_map(|(a, b)| once(a).chain(once(b)))),
                Form::List(l) => Box::new(l.iter()),
                Form::Vec(v) => Box::new(v.iter()),
                _ => Box::new(once(form)),
            },
        }
    }
}

impl<'a> std::iter::Iterator for Iter<'a> {
    type Item = &'a Form;
    fn next(&mut self) -> Option<Self::Item> {
        self.context.next()
    }
}

pub struct IntoIter {
    context: Box<dyn std::iter::Iterator<Item = Form>>,
}

impl IntoIter {
    pub fn new(form: Form) -> Self {
        Self {
            context: match form {
                Form::Map(m) => Box::new(m.into_iter().flat_map(|(a, b)| once(a).chain(once(b)))),
                Form::List(l) => Box::new(l.into_iter()),
                Form::Vec(v) => Box::new(v.into_iter()),
                _ => Box::new(once(form)),
            },
        }
    }
}

impl std::iter::Iterator for IntoIter {
    type Item = Form;
    fn next(&mut self) -> Option<Self::Item> {
        self.context.next()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_iter_form() {
        let form = Form::sym("foo");
        let mut iter = std::iter::once(form);
        assert!(matches!(iter.next(), Some(Some(Form::Symbol(_)))));
    }
}
