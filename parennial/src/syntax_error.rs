#[derive(Debug)]
pub struct SyntaxError {
    what: String,
    error: Option<regex::Error>,
}

#[allow(dead_code)]
pub type Result<T> = std::result::Result<T, SyntaxError>;

impl SyntaxError {
    #[allow(dead_code)]
    pub fn new() -> SyntaxError {
        SyntaxError {
            what: String::from("Something's wrong but nobody knows what"),
            error: None,
        }
    }
}

impl std::convert::From<regex::Error> for SyntaxError {
    fn from(err: regex::Error) -> Self {
        SyntaxError {
            what: String::from("Regex error"),
            error: Some(err),
        }
    }
}

impl std::convert::From<&str> for SyntaxError {
    fn from(err: &str) -> Self {
        SyntaxError {
            what: err.to_string(),
            error: None,
        }
    }
}

impl std::fmt::Display for SyntaxError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "SyntaxError: {}", self.what)
    }
}

impl std::error::Error for SyntaxError {
    fn description(&self) -> &str {
        self.what.as_str()
    }

    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match &self.error {
            Some(err) => Some(err),
            None => None,
        }
    }
}
